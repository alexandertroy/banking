var gulp = require('gulp');


var sourcemaps = require('gulp-sourcemaps');
var minifyCss = require('gulp-minify-css');
//var watch = require('gulp-watch');

var rename = require('gulp-rename');

var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var ts = require('gulp-typescript');

var less = require('gulp-less');

var gutil = require('gulp-util');

var path = {
  app: 'app/Resources',
  bower_components: './bower_components'
};








/**
* gulp-less
* @see https://www.npmjs.com/package/gulp-less
*
* Compile Less to CSS.
*/
gulp.task('less', function() {
    gulp.src(path.app + '/less/main.less')
        .pipe(sourcemaps.init())
        .pipe(less())
        .pipe(minifyCss({keepSpecialComments:0}))
        .pipe(sourcemaps.write())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('web/css/'));
});




/**
* gulp-typescript
* @see https://www.npmjs.com/package/gulp-typescript
*
* Compile Typescript files to Javascript.
*/
gulp.task('ts', function() {
  var tsResult = gulp.src(path.app + '/js/*.ts')
    .pipe(sourcemaps.init())
    .pipe(ts({
      sortOutput: true,
      target: 'ES6', // 'ES3' (default), 'ES5' or 'ES6'.
      module: 'commonjs' //'commonjs' or 'amd'.
    }));

  return tsResult.js
    .pipe(concat('typescript.js'))
    .pipe(uglify({mangle: true}).on('error', gutil.log))
    .pipe(rename({suffix: '.min'}))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('web/js/'));
});



gulp.task('default', [





//'ts',
'less',

]);
