<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\BankAccount;
use AppBundle\Form\BankAccountType;

/**
 * BankAccount controller.
 *
 * @Route("/bankaccount")
 */
class BankAccountController extends Controller
{
    /**
     * Lists all BankAccount entities.
     *
     * @Route("/", name="bankaccount_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $bankAccounts = $em->getRepository('AppBundle:BankAccount')->findAll();

        return $this->render('bankaccount/index.html.twig', array(
            'bankAccounts' => $bankAccounts,
        ));
    }

    /**
     * Creates a new BankAccount entity.
     *
     * @Route("/new", name="bankaccount_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $bankAccount = new BankAccount();
        $form = $this->createForm('AppBundle\Form\BankAccountType', $bankAccount);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($bankAccount);
            $em->flush();

            return $this->redirectToRoute('bankaccount_show', array('id' => $bankAccount->getId()));
        }

        return $this->render('bankaccount/new.html.twig', array(
            'bankAccount' => $bankAccount,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a BankAccount entity.
     *
     * @Route("/{id}", name="bankaccount_show")
     * @Method("GET")
     */
    public function showAction(BankAccount $bankAccount)
    {
        $deleteForm = $this->createDeleteForm($bankAccount);

        return $this->render('bankaccount/show.html.twig', array(
            'bankAccount' => $bankAccount,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing BankAccount entity.
     *
     * @Route("/{id}/edit", name="bankaccount_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, BankAccount $bankAccount)
    {
        $deleteForm = $this->createDeleteForm($bankAccount);
        $editForm = $this->createForm('AppBundle\Form\BankAccountType', $bankAccount);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($bankAccount);
            $em->flush();

            return $this->redirectToRoute('bankaccount_edit', array('id' => $bankAccount->getId()));
        }

        return $this->render('bankaccount/edit.html.twig', array(
            'bankAccount' => $bankAccount,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a BankAccount entity.
     *
     * @Route("/{id}", name="bankaccount_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, BankAccount $bankAccount)
    {
        $form = $this->createDeleteForm($bankAccount);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($bankAccount);
            $em->flush();
        }

        return $this->redirectToRoute('bankaccount_index');
    }

    /**
     * Creates a form to delete a BankAccount entity.
     *
     * @param BankAccount $bankAccount The BankAccount entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(BankAccount $bankAccount)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('bankaccount_delete', array('id' => $bankAccount->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
